<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

require __DIR__ . '/../vendor/autoload.php';

$app = AppFactory::create();

$app->get('/', function (Request $request, Response $response, $args, row $row) {
    $db = new SQLite3(__DIR__.'/../db.sqllite');

    $result = $db->query('SELECT * FROM Comments');

    while($row = $result->fetch_assoc()) ;
    $response->getBody()->write(json_encode($row['id'], $row['result']));
    return $response;
});

$app->run();



