<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

require __DIR__ . '/../vendor/autoload.php';

$app = AppFactory::create();

$app->get('/', function (Request $request, Response $response, $args) {
    $db = new SQLite3(__DIR__.'/../db.sqllite');
    $result = $db->query('SELECT * FROM Comments');

    while($result->fetchArray(SQLITE3_ASSOC)) ;
    $response->getBody()->write(json_encode($result['id'], $result['comment']));
    return $response;
});

$app->run();



